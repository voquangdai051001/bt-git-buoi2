// Bài 1
function tinhtien() {
  var luongNgay = document.getElementById("luongngay").value * 1;
  var soNgay = document.getElementById("songay").value * 1;
  var tongTien = luongNgay * soNgay;
  document.getElementById("ketquabt1").innerHTML = tongTien.toLocaleString();
}

// Bài 2
function tb() {
  var sothuc1 = document.getElementById("stt1").value * 1;
  var sothuc2 = document.getElementById("stt2").value * 1;
  var sothuc3 = document.getElementById("stt3").value * 1;
  var sothuc4 = document.getElementById("stt4").value * 1;
  var sothuc5 = document.getElementById("stt5").value * 1;
  var trungbinhbt2 = (sothuc1 + sothuc2 + sothuc3 + sothuc4 + sothuc5) / 5;
  console.log(trungbinhbt2);
  document.getElementById("ketquabt2").innerHTML = trungbinhbt2;
}

// Bài 3
function quydoi() {
  var soUSDNhap = document.getElementById("soUSD").value * 1;
  var soQuyDoi = soUSDNhap * 23500;
  document.getElementById("ketquabt3").innerHTML = soQuyDoi.toLocaleString();
}

// Bài 4
function tinhkq() {
  var ChieuDai = document.getElementById("chieudai").value * 1;
  var ChieuRong = document.getElementById("chieurong").value * 1;
  var DienTich = ChieuDai * ChieuRong;
  var ChuVi = (ChieuDai + ChieuRong) * 2;
  document.getElementById("ketquaDT").innerHTML = DienTich;
  document.getElementById("ketquaCV").innerHTML = ChuVi

}

// Bài 5
function tong2kyso() {
  var kySo = document.getElementById("stt2cs").value * 1;
  var soHangdv = kySo % 10;
  var soHangChuc = (kySo - soHangdv) / 10;
  var tong2KySo = soHangChuc + soHangdv;
  document.getElementById("ketquabt5").innerHTML = tong2KySo;
}
